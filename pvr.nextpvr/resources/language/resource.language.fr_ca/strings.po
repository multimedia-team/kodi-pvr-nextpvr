# Kodi Media Center language file
# Addon Name: NextPVR PVR Client
# Addon id: pvr.nextpvr
# Addon Provider: Graeme Blackley
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2025-01-17 23:47+0000\n"
"Last-Translator: Christian Gade <gade@kodi.tv>\n"
"Language-Team: French (Canada) <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-nextpvr/fr_ca/>\n"
"Language: fr_ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.9.2\n"

msgctxt "Addon Summary"
msgid "Kodi frontend for the NextPVR"
msgstr "Frontale Kodi pour NextPVR"

msgctxt "Addon Description"
msgid "NextPVR frontend. Supports streaming of Live TV & Recordings, listening to Radio channels and EPG."
msgstr "Frontale de numériscope pour NextPVR, prenant en charge la diffusion en continu des télés en direct et les enregistrements, l’écoute de chaînes radio, et le GÉP."

msgctxt "Addon Disclaimer"
msgid "This is a work in progress. Use at your own risk."
msgstr "Cela est un travail en cours. À utiliser à vos propres risques."

msgctxt "#30000"
msgid "NextPVR hostname"
msgstr ""

msgctxt "#30001"
msgid "NextPVR port (usually 8866)"
msgstr ""

msgctxt "#30002"
msgid "NextPVR PinCode"
msgstr "NIP de NextPVR"

msgctxt "#30003"
msgid "Enable timeshift with live TV"
msgstr ""

msgctxt "#30603"
msgid "Open settings dialog for inputstream.ffmpegdirect for modification of timeshift settings"
msgstr ""

msgctxt "#30004"
msgid "Download TV guide artwork"
msgstr ""

msgctxt "#30504"
msgid "Not enabled when remote"
msgstr ""

msgctxt "#30040"
msgid "Connection"
msgstr "Connexion"

msgctxt "#30041"
msgid "Advanced"
msgstr "Avancé"

# empty strings from id 30042 to 30049
msgctxt "#30050"
msgid "NextPVR version not supported"
msgstr ""

msgctxt "#30051"
msgid "Upgrade to '%s' or higher!"
msgstr "Mettre à niveau vers «  %s » ou ultérieure !"

msgctxt "#30052"
msgid "Connection failed. Incorrect PIN?"
msgstr "Échec de connexion. NIP erroné ?"

msgctxt "#30053"
msgid "Tuner no longer available"
msgstr "Le syntonisateur n’est plus disponible"

msgctxt "#30140"
msgid "One time (manual)"
msgstr "Une fois (manuelle)"

msgctxt "#30141"
msgid "One time (guide)"
msgstr "Une fois (guide)"

msgctxt "#30142"
msgid "Repeating (manual)"
msgstr "Répétition (manuelle)"

msgctxt "#30143"
msgid "Repeating (guide)"
msgstr "Répétition (guide)"

msgctxt "#30144"
msgid "Child recording"
msgstr ""

msgctxt "#30145"
msgid "Repeating (keyword)"
msgstr "Répétition (mot-clef)"

msgctxt "#30150"
msgid "All Recordings"
msgstr "Tous les enregistrements"

msgctxt "#30151"
msgid "1 Recording"
msgstr "1 enregistrement"

msgctxt "#30152"
msgid "2 Recordings"
msgstr "2 enregistrements"

msgctxt "#30153"
msgid "3 Recordings"
msgstr "3 enregistrements"

msgctxt "#30154"
msgid "4 Recordings"
msgstr "4 enregistrements"

msgctxt "#30155"
msgid "5 Recordings"
msgstr "5 enregistrements"

msgctxt "#30156"
msgid "6 Recordings"
msgstr "6 enregistrements"

msgctxt "#30157"
msgid "7 Recordings"
msgstr "7 enregistrements"

msgctxt "#30158"
msgid "10 Recordings"
msgstr "10 enregistrements"

msgctxt "#30160"
msgid "New Only"
msgstr "Nouveaux seulement"

msgctxt "#30161"
msgid "All"
msgstr "Tout"

msgctxt "#30162"
msgid "Pre-buffer time in seconds"
msgstr ""

msgctxt "#30163"
msgid "Enable Wake On LAN"
msgstr ""

msgctxt "#30164"
msgid "Wait for server after wake in seconds"
msgstr ""

msgctxt "#30165"
msgid "NextPVR server MAC address"
msgstr ""

msgctxt "#30166"
msgid "Failed recordings"
msgstr ""

msgctxt "#30167"
msgid "Live TV chunk size"
msgstr ""

msgctxt "#30567"
msgid "Change chunk size for LAN performance"
msgstr ""

msgctxt "#30168"
msgid "Recording chunk size"
msgstr ""

msgctxt "#30170"
msgid "Refresh cache for channel icons"
msgstr "Rafraîchir le cache des icônes de chaînes"

msgctxt "#30171"
msgid "Repeating (advanced)"
msgstr ""

msgctxt "#30172"
msgid "Live TV method"
msgstr ""

msgctxt "#30173"
msgid "Transcoding profile"
msgstr ""

msgctxt "#30174"
msgid "Streaming options"
msgstr ""

msgctxt "#30175"
msgid "Display and recordings"
msgstr ""

msgctxt "#30176"
msgid "Enable direct streams"
msgstr ""

msgctxt "#30177"
msgid "Use legacy (V4) setting"
msgstr ""

msgctxt "#30178"
msgid "Include filesize with recordings"
msgstr ""

msgctxt "#30678"
msgid "Use file system to get the recording size if available"
msgstr ""

msgctxt "#30179"
msgid "Remote access"
msgstr ""

msgctxt "#30180"
msgid "Single recording display"
msgstr ""

msgctxt "#30680"
msgid "Movies and single episode TV shows display at top level"
msgstr ""

msgctxt "#30181"
msgid "TV guide portrait format"
msgstr ""

msgctxt "#30182"
msgid "NextPVR server %s:%d"
msgstr ""

msgctxt "#30183"
msgid "Reset channel icon"
msgstr ""

msgctxt "#30184"
msgid "Forget recording"
msgstr ""

msgctxt "#30185"
msgid "Update all channels"
msgstr ""

msgctxt "#30186"
msgid "Update all channel groups"
msgstr ""

msgctxt "#30187"
msgid "Select NextPVR IP address"
msgstr ""

msgctxt "#30188"
msgid "Show radio channels"
msgstr ""

msgctxt "#30688"
msgid "Also use skin settings to control radio main menu display"
msgstr ""

msgctxt "#30189"
msgid "Discovered host"
msgstr ""

msgctxt "#30190"
msgid "Timeshifting disabled"
msgstr ""

msgctxt "#30191"
msgid "%s is not enabled"
msgstr ""

msgctxt "#30192"
msgid "%s is not installed"
msgstr ""

msgctxt "#30193"
msgid "Show season folders"
msgstr ""

msgctxt "#30693"
msgid "Create separate folders for multiple seasons of recordings"
msgstr ""

msgctxt "#30194"
msgid "Use backend genre text"
msgstr ""

msgctxt "#30694"
msgid "Kodi default is DVB genre text. Default required for Kodi genre search and language translation"
msgstr ""

msgctxt "#30195"
msgid "Send Wake On LAN to server"
msgstr ""

msgctxt "#30196"
msgid "Open NextPVR settings"
msgstr ""

msgctxt "#30197"
msgid "Show guide cast, director and writer"
msgstr ""

msgctxt "#30198"
msgid "Display disk space"
msgstr ""

msgctxt "#30698"
msgid "Default: Show the default drive - Span: Sum all drives"
msgstr ""

msgctxt "#30199"
msgid "Override Kodi no timer padding default"
msgstr ""

msgctxt "#30699"
msgid "Use NextPVR timer padding when no padding is entered"
msgstr ""

msgctxt "#30200"
msgid "Store recording resume location in NextPVR"
msgstr ""

msgctxt "#30700"
msgid "When disabled resume location and watched status will be managed only in Kodi"
msgstr ""

msgctxt "#30201"
msgid "Show root directory"
msgstr ""

msgctxt "#30701"
msgid "Seperate recordings by the NextPVR recording folder"
msgstr ""

msgctxt "#30202"
msgid "Channel instance number"
msgstr ""

msgctxt "#30702"
msgid "Add instance indicators (#) to channel names"
msgstr ""

msgctxt "#30203"
msgid "All channels group"
msgstr ""

msgctxt "#30703"
msgid "Create an All channels group for this instance"
msgstr ""

msgctxt "#30204"
msgid "Instance number"
msgstr ""

msgctxt "#30205"
msgid "Instance options"
msgstr ""

msgctxt "#30206"
msgid "Load comskip"
msgstr ""

msgctxt "#30706"
msgid "Read comskip EDL files from backend"
msgstr ""

msgctxt "#30207"
msgid "Update check"
msgstr ""

msgctxt "#30707"
msgid "Interval to check for backend changes made outside this Kodi client"
msgstr ""

msgctxt "#30208"
msgid "Every 5 minutes"
msgstr ""

msgctxt "#30209"
msgid "Recording and timer control"
msgstr ""

msgctxt "#30709"
msgid "Manage user rights for recordings and timers"
msgstr ""

msgctxt "#30210"
msgid "Real Time"
msgstr ""

msgctxt "#30211"
msgid "Timeshift"
msgstr ""

msgctxt "#30212"
msgid "Transcoded"
msgstr ""

msgctxt "#30213"
msgid "Spann"
msgstr ""

msgctxt "#30214"
msgid "Full access to recordings and timers"
msgstr ""

msgctxt "#30215"
msgid "Play and delete recordings with no timers"
msgstr ""

msgctxt "#30216"
msgid "Play recordings with no timers"
msgstr ""

msgctxt "#30217"
msgid "Live TV only"
msgstr ""

msgctxt "#30218"
msgid "Repeating (all episodes)"
msgstr ""

#~ msgctxt "#30000"
#~ msgid "NextPVR Hostname"
#~ msgstr "Nom d’hôte de NextPVR"

#~ msgctxt "#30001"
#~ msgid "NextPVR Port (usually 8866 or 7799)"
#~ msgstr "Port de NextPVR (habituellement 8866 ou 7799)"

#~ msgctxt "#30003"
#~ msgid "Enable TimeShift with Live TV"
#~ msgstr "Activer le décalage temporel avec la télé en direct"

#~ msgctxt "#30004"
#~ msgid "Download TV Guide Artwork"
#~ msgstr "Télécharger les illustrations du guide télé"

#~ msgctxt "#30050"
#~ msgid "NextPVR server is too old."
#~ msgstr "Le serveur NextPVR est trop ancien."

#~ msgctxt "#30144"
#~ msgid "Child Recording"
#~ msgstr "Enregistrement enfant"
